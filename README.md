# RCSystems Test Project

### Prerequisites
In case you are not familiar with git I leave here detailed instruction

I Use STM32CubeIDE to compile and debug project
- [STM32CubeIDE](https://www.st.com/en/development-tools/stm32cubeide.html) - Integrated Development Environment (IDE) for STM32 microcontrollers.
- [STM32Cube firmware package](https://www.st.com/en/embedded-software/stm32cube.html) - Firmware package for STM32 microcontrollers.

### Installation

1. Clone the repository:

   ```shell
   git config --global user.name "Your Username"
   git config --global user.email "your-email@example.com"
   git clone https://gitlab.com/proxv/rcsystemstest
   ```

2. Open STM32CubeIDE.

3. Import the project:
    - From the menu, go to `File` -> `Import`.
    - Select `General` -> `Existing Projects into Workspace`.
    - Choose the root directory where you cloned the repository.
    - Select the project from the list.
    - Click `Finish`.

4. Build the project:
    - Right-click on the project in the Project Explorer.
    - Select `Build Project`.

### Usage

1. Connect your STM32 Nucleo board to your computer via USB.

2. Build and flash the project onto the board:
    - Right-click on the project in the Project Explorer.
    - Select `Build Project`.
    - Once the build process completes, right-click on the project again.
    - Select `Debug As` -> `STM32 MCU Debugging`.

3. Open a serial terminal program (Putty) and connect to the board's serial port using the appropriate settings (COM#, baud rate 115200).

4. Run the program on the board:
    - In STM32CubeIDE, click the `Resume` button or press `F8` to start the program execution.
    - The program will run on the board, and the temperature readings will be displayed in the serial terminal.

